# include<fstream>
# include<iostream>
using namespace std;
struct Point
{
	float key;
	float attachment;
};

template <typename T>
class BinarySearch
{
public:
	BinarySearch(ifstream &infile, int max = 50000);
	~BinarySearch() { delete[] head; };
	bool BinSearch(T mean, T window);
	bool Read(Point *&point);
	int count;
private:
	int BinSearch_Pri(T mean, T window);
	Point *head;
	int CurrentSize;
	int MaxSize;
	int indexl;
	int indexh;
};

void get_abc(float x3, float x4, float x5, float * &solution);
bool decide(float x3, float x4, float x5, float middle);
int find(BinarySearch<float> &data, float mean, float window, Point *&read_data);
int find(Point *& read_data, float mean, float window, Point *&output_data, int number);

int work10()
{
	TFile *f = TFile::Open("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");
	
	Point *read_data_y[5] = { NULL, NULL, NULL, NULL, NULL };
	Point *read_data_x[3] = { NULL, NULL, NULL };
	ifstream infiley1("/afs/cern.ch/user/j/jiazhen/private/result/layer1 y hits.txt", ios::in);
	ifstream infiley2("/afs/cern.ch/user/j/jiazhen/private/result/clayer2 y hits.txt", ios::in);
	ifstream infiley3("/afs/cern.ch/user/j/jiazhen/private/result/clayer3 y hits.txt", ios::in);
	ifstream infiley4("/afs/cern.ch/user/j/jiazhen/private/result/clayer4 y hits.txt", ios::in);
	ifstream infiley5("/afs/cern.ch/user/j/jiazhen/private/result/clayer5 y hits.txt", ios::in);
	ifstream infilex1("/afs/cern.ch/user/j/jiazhen/private/result/clayer1 x hits.txt", ios::in);
	ifstream infilex2("/afs/cern.ch/user/j/jiazhen/private/result/clayer2 x hits.txt", ios::in);
	ifstream infilex3("/afs/cern.ch/user/j/jiazhen/private/result/clayer3 x hits.txt", ios::in);
	BinarySearch<float> datay5(infiley5);
	BinarySearch<float> datay4(infiley4);
	BinarySearch<float> datay3(infiley3);
	BinarySearch<float> datay2(infiley2);
	BinarySearch<float> datay1(infiley1);
	BinarySearch<float> datax3(infilex3);
	BinarySearch<float> datax2(infilex2);
	BinarySearch<float> datax1(infilex1);

	float hitzpos[30], hitypos[30], hitxpos[30], middle ;
	float x[6] = { 0, 0, 0, 0, 0, 0 };
	float y[6] = { 0, 0, 0, 0, 0, 0 };
	float pre_x[3] = { 0, 0, 0 };
	float pre_y[5] = { 0, 0, 0, 0, 0 };
	float *solution = new float[3];
	int nentries, i, j, iter_count, iter_flag, iter_x, iter_y;
	int cut_number = 0, abnormal_number = 0, not_in_the_same_side = 0, not_reach_the_first_layer = 0;
	int count[7] = { 0, 0, 0, 0, 0, 0, 0 };//0-total 1 2 3 4 5 6-layer
	bool flag[7] = { 0, 0, 0, 0, 0, 0, 0 };
	double eta, p, pz;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitXpos", hitxpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	tracks->SetBranchAddress("p", &p);
	tracks->SetBranchAddress("pz", &pz);
	nentries = tracks->GetEntries();

	int request = 100;
	do
	{
		cout << "Enter the number: ";
		cin >>request;
		if (request <= 0)
			break;
		for (i = 0; i < nentries; i++)
		{
			tracks->GetEntry(i);
			for (j = 0; j < 30; j++)
			{
				if (hitzpos[j] > 7816 && hitzpos[j] < 7836)
				{
					count[1]++;
					x[0] = x[0] + hitxpos[j];
					y[0] = y[0] + hitypos[j];
					flag[1] = 1;
				}
				else if (hitzpos[j] > 8024 && hitzpos[j] < 8047)
				{
					count[2]++;
					x[1] = x[1] + hitxpos[j];
					y[1] = y[1] + hitypos[j];
					flag[2] = 1;
				}
				else if (hitzpos[j] > 8497 && hitzpos[j] < 8518)
				{
					count[3]++;
					x[2] = x[2] + hitxpos[j];
					y[2] = y[2] + hitypos[j];
					flag[3] = 1;
				}
				else if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
				{
					count[4]++;
					x[3] = x[3] + hitxpos[j];
					y[3] = y[3] + hitypos[j];
					flag[4] = 1;
				}
				else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
				{
					count[5]++;
					x[4] = x[4] + hitxpos[j];
					y[4] = y[4] + hitypos[j];
					flag[5] = 1;
				}
				else if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
				{
					count[6]++;
					x[5] = x[5] + hitxpos[j];
					y[5] = y[5] + hitypos[j];
					flag[6] = 1;
				}
			}
			eta = 0.5*log((p + pz) / (p - pz));
			if (p > 5 && (eta > 2 && eta < 5))
			{
				cut_number++;
				if (flag[1] && flag[2] && flag[3] && flag[4] && flag[5] && flag[6])
				{
					if (count[1] > 1 || count[2] > 1 || count[3] > 1 || count[4] > 1 || count[5] > 1 || count[6] > 1)
					{
						abnormal_number++;
					}
					else
					{
						for (iter_x = 0; iter_x < 6; iter_x++)
							x[iter_x] = x[iter_x] / count[iter_x + 1];
						for (iter_y = 0; iter_y < 6; iter_y++)
							y[iter_y] = y[iter_y] / count[iter_y + 1];

						get_abc(x[3], x[4], x[5], solution);//solution [0]-a [1]-b [2]-c
						middle = -(solution[1] / (2 * solution[0]));
						if (decide(x[3], x[4], x[5], middle))
						{
							not_in_the_same_side++;
						}
						else  if (((solution[0] * middle*middle + solution[1] * middle + solution[2]) > 7826) && (solution[0] > 0))
						{
							not_reach_the_first_layer++;
						}
						else
						{
							count[0]++;
							if ((x[5] > middle && solution[0] > 0) || (x[5] < middle && solution[0] < 0))
								pre_x[0] = middle + (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 7826)) / (2 * solution[0]));
							else
								pre_x[0] = middle - (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 7826)) / (2 * solution[0]));
							if ((x[5] > middle && solution[0] > 0) || (x[5] < middle && solution[0] < 0))
								pre_x[1] = middle + (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8035)) / (2 * solution[0]));
							else
								pre_x[1] = middle - (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8035)) / (2 * solution[0]));
							if ((x[5] > middle && solution[0] > 0) || (x[5] < middle && solution[0] < 0))
								pre_x[2] = middle + (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8508)) / (2 * solution[0]));
							else
								pre_x[2] = middle - (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8508)) / (2 * solution[0]));
							pre_y[0] = (1576.0 / 209.0)*y[4] - (1367.0 / 209.0)*y[5];
							pre_y[1] = (1367.0 / 209.0)*y[4] - (1158.0 / 209.0)*y[5];
							pre_y[2] = (894.0 / 209.0)*y[4] - (685.0 / 209.0)*y[5];
							pre_y[3] = (685.0 / 209.0)*y[4] - (476.0 / 209.0)*y[5];
							pre_y[4] = y[5];

							if (count[0] == request)
								break;
						}
					}
				}
			}
			for (iter_x = 0; iter_x < 6; iter_x++)
				x[iter_x] = 0;
			for (iter_y = 0; iter_y < 6; iter_y++)
				y[iter_y] = 0;
			for (iter_x = 0; iter_x < 3; iter_x++)
				pre_x[iter_x] = 0;
			for (iter_y = 0; iter_y < 5; iter_y++)
				pre_y[iter_y] = 0;
			for (iter_count = 1; iter_count < 7; iter_count++)
				count[iter_count] = 0;
			for (iter_flag = 1; iter_flag < 7; iter_flag++)
				flag[iter_flag] = 0;
		}
		cout << "6th layer point(x, y):" << x[5] << ", " << y[5] << endl;
		cout << "5th layer poing(x, y):" << x[4] << ", " << y[4] << endl;
		cout << "4th layer point(x, y):" << x[3] << ", " << y[3] << "  " << pre_y[3] << endl;
		cout << "3rd layer poing(x, y):" << x[2] << ", " << y[2] << "  " << pre_x[2] << ", " << pre_y[2] << endl;
		cout << "2nd layer point(x, y):" << x[1] << ", " << y[1] << "  " << pre_x[1] << ", " << pre_y[1] << endl;
		cout << "1st layer poing(x, y):" << x[0] << ", " << y[0] << "  " << pre_x[0] << ", " << pre_y[0] << endl;
		cout << "layer y5: " << find(datay5, pre_y[4], 48.83, read_data_y[4]) << endl;
		cout << "layer y4: " << find(datay4, pre_y[3], 0.9455, read_data_y[3]) << endl;
		cout << "layer y3: " << find(datay3, pre_y[2], 1.168, read_data_y[2]) << endl;
		cout << "layer y2: " << find(datay2, pre_y[1], 2.272, read_data_y[1]) << endl;
		cout << "layer y1: " << find(datay1, pre_y[0], 2.518, read_data_y[0]) << endl;
		
		cout << "layer x3: " << find(read_data_y[2], pre_x[2], 2.5135, read_data_x[2], datay3.count) << endl;
		cout << "layer x2: " << find(read_data_y[1], pre_x[1], 3.6875, read_data_x[1], datay2.count) << endl;
		cout << "layer x1: " << find(read_data_y[0], pre_x[0], 5.28, read_data_x[0], datay1.count) << endl;

		for (iter_x = 0; iter_x < 3; iter_x++)
		{
			delete[] read_data_x[iter_x];
			read_data_x[iter_x] = NULL;
		}
		for (iter_y = 0; iter_y < 5; iter_y++)
		{
			delete[] read_data_y[iter_y];
			read_data_y[iter_y] = NULL;
		}
		for (iter_x = 0; iter_x < 6; iter_x++)
			x[iter_x] = 0;
		for (iter_y = 0; iter_y < 6; iter_y++)
			y[iter_y] = 0;
		for (iter_x = 0; iter_x < 3; iter_x++)
			pre_x[iter_x] = 0;
		for (iter_y = 0; iter_y < 5; iter_y++)
			pre_y[iter_y] = 0;
		for (iter_count = 1; iter_count < 7; iter_count++)
			count[iter_count] = 0;
		for (iter_flag = 1; iter_flag < 7; iter_flag++)
			flag[iter_flag] = 0;
		count[0] = 0;
		cut_number = 0;
		abnormal_number = 0;
		not_in_the_same_side = 0;
		not_reach_the_first_layer = 0;
	}	while (1);


	infiley1.close();
	infiley2.close();
	infiley3.close();
	infiley4.close();
	infiley5.close();
	infilex1.close();
	infilex2.close();
	infilex3.close();
	f->Close();
	delete[] solution;
	return 0;
}

int find(Point *& read_data, float mean, float window, Point *&output_data, int number)
{
	int count = 0, j = 0, i;
	int *note = new int[number + 1];
	float lowerwindow = mean - window;
	float upperwindow = mean + window;
	for (i = 0; i < number; i++)
	{
		if (read_data[i].attachment > lowerwindow && read_data[i].attachment < upperwindow)
		{
			count++;
			note[j] = i;
			j++;
		}
	}
	if (count != 0)
	{
		note[j] = -1;
		output_data = new Point[count];
		for (j = 0; note[j] == -1; j++)
		{
			output_data[j].attachment = read_data[note[j]].attachment;
			output_data[j].key = read_data[note[j]].key;
		}
	}
	delete[] note;
	return count;
}

int find(BinarySearch<float> &data, float mean, float window, Point *&read_data)
{
	int count = 0;
	if (data.BinSearch(mean, window))
	{
		data.Read(read_data);
		int i;
		count = data.count;
		//for (i = 0; i < data.count; i++)
		//{
		//	count++;
		//}
	}
	else
	{
		cout << "false";
	}
	return count;
}

template <typename T>
BinarySearch <T>::BinarySearch(ifstream &infile, int max)
{
	int index;
	head = new Point[max];
	infile >> CurrentSize;
	for (index = 0; index < max; index++)
	{
		infile >> head[index].key;
		infile >> head[index].attachment;
	}
	MaxSize = max;
}

template <typename T>
int BinarySearch <T>::BinSearch_Pri(T mean, T window)
{
	int low = 0, high = CurrentSize - 1, mid;
	T upperwindow, lowerwindow;
	upperwindow = mean + window;
	lowerwindow = mean - window;
	while (low <= high)
	{
		mid = low + (high - low) / 2;
		if ((head[low].key > lowerwindow) && (head[low].key < upperwindow))
		{
			return low;
		}
		else if ((head[high].key > lowerwindow) && (head[high].key < upperwindow))
		{
			return high;
		}
		else if ((head[mid].key > lowerwindow) && (head[mid].key < upperwindow))
		{
			return mid;
		}
		else if (head[mid].key <= lowerwindow)
		{
			low = mid + 1;
		}
		else if (head[mid].key >= upperwindow)
		{
			high = mid - 1;
		}
	}
	return -1;
}

template <typename T>
bool BinarySearch <T>::BinSearch(T mean, T window)
{
	int index, temp;
	T lowerwindow, upperwindow;
	lowerwindow = mean - window;
	upperwindow = mean + window;
	index = BinSearch_Pri(mean, window);
	if (index == -1)
	{
		return false;
	}
	else
	{
		for (temp = index; (head[temp].key > lowerwindow) && (temp >= 0); temp--);
		indexl = temp + 1;
		for (temp = index; (head[temp].key < upperwindow) && (temp <= (CurrentSize - 1)); temp++);
		indexh = temp - 1;
		count = indexh - indexl + 1;
		return true;
	}
}

template <typename T>
bool BinarySearch <T>::Read(Point *&point)
{
	if ((indexh <= CurrentSize) && (indexl >= 0))
	{
		int i;
		point = new Point[count];
		for (i = 0; i < count; i++)
		{
			point[i].key = head[indexl + i].key;
			point[i].attachment = head[indexl + i].attachment;
		}
		return true;
	}
	else
		return false;
}

// [0]-a [1]-b [2]-c
void get_abc(float x3, float x4, float x5, float * &solution)
{
	//c
	solution[2] = (8717.0*x5*x5 - 9402.0*x3*x3 - (9193 * x5*x5 - 9402 * x4*x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4)) / (x5*x5 - x3 * x3 - (x5*x5 - x4 * x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4));
	//b
	solution[1] = (1 / (x4 - x4 * x4 / x5))*(9193 - 9402 * x4 * x4 / (x5 * x5) - (1 - x4 * x4 / (x5 * x5)) * solution[2]);
	//a
	solution[0] = (1 / (x5*x5))*(9402 - x5 * solution[1] - solution[2]);
}

bool decide(float x3, float x4, float x5, float middle)
{
	bool com1 = 0, com2 = 0, com3 = 0;
	if (x3 > middle)
		com1 = 1;
	else
		com1 = 0;
	if (x4 > middle)
		com2 = 1;
	else
		com2 = 0;
	if (x5 > middle)
		com3 = 1;
	else
		com3 = 0;
	return (com1 || com2 || com3) && (!(com1 && com2 && com3));
}
