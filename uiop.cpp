# include<iostream>
# include<ctime>
# include<fstream>
# include<vector>
# include<cmath>

using namespace std;

struct Point
{
	float key;
	float attachment;
};

struct Read__data
{
	Point *Read_data;
	int count;
};

class Point6th
{
public:

	int entry;
	bool have_point;
	vector<vector<float>> solution;
	vector<vector<vector<Point>>> point_1st_2nd_3rd;//[0]-index [1]-0_1st 1_2nd 2_3rd [2]-Point
	vector<Point> Point5th;
	vector<Point> Point4th;
};

template <typename T>
class BinarySearch
{
public:
	BinarySearch(ifstream &infile, int max = 50000);
	~BinarySearch() { delete[] head; };
	bool BinSearch(T mean, T window);
	bool Read(Point *&point);
	int count;
private:
	int BinSearch_Pri(T mean, T window);
	Point *head;
	int CurrentSize;
	int MaxSize;
	int indexl;
	int indexh;
};

void get_abc(float x3, float x4, float x5, float * &solution);
bool decide(float x3, float x4, float x5, float middle);
int find(BinarySearch<float> &data, float mean, float window, Point *&read_data);
int find(Point *& read_data, float mean, float window, Point *&output_data, int number);

int myrandom(int n);
bool happened(double probability);
const double MinProb = 1.0 / (long(RAND_MAX) + 1);

int uiop()
{
	TFile *f = TFile::Open("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");

	Point *read_data_y[5] = { NULL, NULL, NULL, NULL, NULL };
	Read__data Read_data_x[3];
	Read_data_x[0].Read_data = NULL;
	Read_data_x[1].Read_data = NULL;
	Read_data_x[2].Read_data = NULL;
	//ifstream infiley1("/afs/cern.ch/user/j/jiazhen/private/result/layer1 y hits.txt", ios::in);
	//ifstream infiley2("/afs/cern.ch/user/j/jiazhen/private/result/layer2 y hits.txt", ios::in);
	//ifstream infiley3("/afs/cern.ch/user/j/jiazhen/private/result/layer3 y hits.txt", ios::in);
	//ifstream infiley4("/afs/cern.ch/user/j/jiazhen/private/result/layer4 y hits.txt", ios::in);
	//ifstream infiley5("/afs/cern.ch/user/j/jiazhen/private/result/layer5 y hits.txt", ios::in);

	ifstream infiley1("layer1 y hits.txt", ios::in);
	ifstream infiley2("layer2 y hits.txt", ios::in);
	ifstream infiley3("layer3 y hits.txt", ios::in);
	ifstream infiley4("layer4 y hits.txt", ios::in);
	ifstream infiley5("layer5 y hits.txt", ios::in);
	ifstream input("search entry number.txt", ios::in);
	BinarySearch<float> datay5(infiley5);
	BinarySearch<float> datay4(infiley4);
	BinarySearch<float> datay3(infiley3);
	BinarySearch<float> datay2(infiley2);
	BinarySearch<float> datay1(infiley1);

	float hitzpos[30], hitypos[30], hitxpos[30], middle;
	float x[6] = { 0, 0, 0, 0, 0, 0 };
	float y[6] = { 0, 0, 0, 0, 0, 0 };
	float pre_x[3] = { 0, 0, 0 };
	float pre_y[5] = { 0, 0, 0, 0, 0 };
	float *solution = new float[3];
	int nentries, entry, i, j, k, iter_fillpoints, iter_count, iter_flag, iter_x, iter_y;
	int possible_find_count = 0;
	int not_found = 0;
	bool find_flag[3] = { false, false, false }, point_have = true;
	double eta, p, pz;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitXpos", hitxpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	tracks->SetBranchAddress("p", &p);
	tracks->SetBranchAddress("pz", &pz);
	nentries = tracks->GetEntries();

	srand(time(0));
	Point6th mypoint[41873];
	vector<float> fill_in_solution;
	vector<Point> index_of_the_point;
	vector<vector<Point>> index_of_the_layer;
	float hit_res_x = (0.1 / sqrt(12));
	float hit_res_y = (0.4 / sqrt(12));

	for (i = 0; i < 1000; i++)
	{	
		cout << "i = " << i << endl;
		//find the entry that has point on 6th layer
		//while (point_have)
		//{	
		//	entry = myrandom(nentries);
		//	tracks->GetEntry(entry);
		//	cout << entry << endl;
		//	for (j = 0; j < 30; j++)
		//	{	
		//		if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
		//		{
		//			x[5] = hitxpos[j];
		//			y[5] = hitypos[j];
		//			point_have = false;
		//			break;
		//		}
		//	}
		//} 
		input >> entry;
		input >> x[5];
		input >> y[5];

		mypoint[i].entry = entry;
		point_have = true;

		//find the possible combination on 5th layer
		pre_y[4] = y[5];
		find(datay5, pre_y[4], 48.83, read_data_y[4]);//delete on line 307-308
		//not finished
		if (datay5.count == 0)
		{

		}
		else
		{
			//process the points on layer 5 one by one
			for (j = 0; j < datay5.count; j++)
			{
				y[4] = read_data_y[4][j].key;
				x[4] = read_data_y[4][j].attachment;

				//predict point on layer 4 and search it
				pre_y[3] = (685.0 / 209.0)*y[4] - (476.0 / 209.0)*y[5];
				find(datay4, pre_y[3], 0.9455, read_data_y[3]); //delete on line 302-303 

				//not finished
				if (datay4.count == 0)
				{

				}
				else
				{
					//process the points on layer 4 one by one
					for (k = 0; k < datay4.count; k++)
					{
						y[3] = read_data_y[3][k].key;
						x[3] = read_data_y[3][k].attachment;
						
						//decide if they meet the requirement of the x component
						get_abc(x[3], x[4], x[5], solution);//solution [0]-a [1]-b [2]-c
						middle = -(solution[1] / (2 * solution[0]));
						//not in the same side
						//not finished
						if (decide(x[3], x[4], x[5], middle))
						{
							not_found++;
						}
						//not reach the 1st layer
						//not finished
						else  if (((solution[0] * middle*middle + solution[1] * middle + solution[2]) > 7826) && (solution[0] > 0))
						{
							not_found++;
						}
						else
						{
							//predict point on layer 3, 2 and 1
							if ((x[5] > middle && solution[0] > 0) || (x[5] < middle && solution[0] < 0))
								pre_x[0] = middle + (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 7826)) / (2 * solution[0]));
							else
								pre_x[0] = middle - (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 7826)) / (2 * solution[0]));
							if ((x[5] > middle && solution[0] > 0) || (x[5] < middle && solution[0] < 0))
								pre_x[1] = middle + (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8035)) / (2 * solution[0]));
							else
								pre_x[1] = middle - (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8035)) / (2 * solution[0]));
							if ((x[5] > middle && solution[0] > 0) || (x[5] < middle && solution[0] < 0))
								pre_x[2] = middle + (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8508)) / (2 * solution[0]));
							else
								pre_x[2] = middle - (sqrt(solution[1] * solution[1] - 4 * solution[0] * (solution[2] - 8508)) / (2 * solution[0]));
							pre_y[0] = (1576.0 / 209.0)*y[4] - (1367.0 / 209.0)*y[5];
							pre_y[1] = (1367.0 / 209.0)*y[4] - (1158.0 / 209.0)*y[5];
							pre_y[2] = (894.0 / 209.0)*y[4] - (685.0 / 209.0)*y[5];

							//search predicted point on layer 3, 2 and 1
							find(datay3, pre_y[2], 1.168, read_data_y[2]);//delete on the line 288-297
							find(datay2, pre_y[1], 2.272, read_data_y[1]);
							find(datay1, pre_y[0], 2.518, read_data_y[0]);
							Read_data_x[2].count = find(read_data_y[2], pre_x[2], 2.5135, Read_data_x[2].Read_data, datay3.count);
							Read_data_x[1].count = find(read_data_y[1], pre_x[1], 3.6875, Read_data_x[1].Read_data, datay2.count);
							Read_data_x[0].count = find(read_data_y[0], pre_x[0], 5.28, Read_data_x[0].Read_data, datay1.count);

							if (Read_data_x[2].count != 0)
							{
								find_flag[0] = true;
							}
							if (Read_data_x[1].count != 0)
							{
								find_flag[1] = true;
							}
							if (Read_data_x[0].count != 0)
							{
								find_flag[2] = true;
							} 

							//push the information into mypoint[i]
							if (find_flag[0] && find_flag[1] && find_flag[2])
							{
								//initialize the solution
								fill_in_solution.push_back(solution[0]);
								fill_in_solution.push_back(solution[1]);
								fill_in_solution.push_back(solution[2]);
								mypoint[i].solution.push_back(fill_in_solution);
								fill_in_solution.clear();

								//initialize Point5th and Point4th
								mypoint[i].Point4th.push_back(read_data_y[3][k]);
								mypoint[i].Point5th.push_back(read_data_y[4][j]);

								//initialize Point 1st
								for (iter_fillpoints = 0; iter_fillpoints < Read_data_x[0].count; iter_fillpoints++)
								{
									index_of_the_point.push_back(Read_data_x[0].Read_data[iter_fillpoints]);
								}
								index_of_the_layer.push_back(index_of_the_point);
								index_of_the_point.clear();

								//initialize Point 2nd
								for (iter_fillpoints = 0; iter_fillpoints < Read_data_x[1].count; iter_fillpoints++)
								{
									index_of_the_point.push_back(Read_data_x[1].Read_data[iter_fillpoints]);
								}
								index_of_the_layer.push_back(index_of_the_point);
								index_of_the_point.clear();

								//initialize Point 3rd
								for (iter_fillpoints = 0; iter_fillpoints < Read_data_x[2].count; iter_fillpoints++)
								{
									index_of_the_point.push_back(Read_data_x[2].Read_data[iter_fillpoints]);
								}
								index_of_the_layer.push_back(index_of_the_point);
								index_of_the_point.clear();

								mypoint[i].point_1st_2nd_3rd.push_back(index_of_the_layer);
								index_of_the_layer.clear();
							}
							//not finished
							else
							{
								not_found++;
							}

							for (iter_x = 0; iter_x < 3; iter_x++)
							{
								delete[] Read_data_x[iter_x].Read_data;
								Read_data_x[iter_x].Read_data = NULL;
							}
							for (iter_y = 0; iter_y < 3; iter_y++)
							{
								delete[] read_data_y[iter_y];
								read_data_y[iter_y] = NULL;
							}
							for (iter_flag = 0; iter_flag < 3; iter_flag++)
								find_flag[iter_flag] = false;
						}
					}

					if (not_found == datay4.count)
					{

					}
					not_found = 0;
				}

				delete[] read_data_y[3];
				read_data_y[3] = NULL;
			}
		}

		delete[] read_data_y[4];
		read_data_y[4] = NULL;

		//decide if there is the track that reconstracted
		if (mypoint[i].Point5th.size() > 0)
		{
			mypoint[i].have_point = true;
		}
		else
		{
			mypoint[i].have_point = false;
		}
	}

	int t, reconstructed = 0;
	bool flag[5] = { false, false, false, false, false };
	vector<float> real_x_hits[5];//可以用point
	vector<float> real_y_hits[5];
	for (i = 0; i < 1000; i++)
	{
		tracks->GetEntry(mypoint[i].entry);

		// calculate reconstructed tracks
		if (mypoint[i].have_point)
		{
			reconstructed++;
		}

		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 7816 && hitzpos[j] < 7836)
			{
				real_x_hits[0].push_back(hitxpos[j]);
				real_y_hits[0].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 8024 && hitzpos[j] < 8047)
			{
				real_x_hits[1].push_back(hitxpos[j]);
				real_y_hits[1].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 8497 && hitzpos[j] < 8518)
			{
				real_x_hits[2].push_back(hitxpos[j]);
				real_y_hits[2].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				real_x_hits[3].push_back(hitxpos[j]);
				real_y_hits[3].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
			{
				real_x_hits[4].push_back(hitxpos[j]);
				real_y_hits[4].push_back(hitypos[j]);
			}
		}
		for (j = 0; j < mypoint[i].Point5th.size(); j++)
		{
			for (k = 0; k < real_x_hits[4].size(); k++)
			{
				if ((fabs(real_x_hits[4][k] - mypoint[i].Point5th[j].attachment) < hit_res_x) && (fabs(real_y_hits[4][k] - mypoint[i].Point5th[j].key) < hit_res_y))
				{
					flag[4] = true;
				}
			}
			for (k = 0; k < real_x_hits[3].size(); k++)
			{
				if ((fabs(real_x_hits[3][k] - mypoint[i].Point4th[j].attachment) < hit_res_x) && (fabs(real_y_hits[3][k] - mypoint[i].Point4th[j].key) < hit_res_y))
				{
					flag[3] = true;
				}
			}
			for (k = 0; k < real_x_hits[2].size(); k++)
			{
				for (t = 0; t < mypoint[i].point_1st_2nd_3rd[j][2].size(); t++)
				{
					if ((fabs(real_x_hits[2][k] - mypoint[i].point_1st_2nd_3rd[j][2][t].attachment) < hit_res_x) && (fabs(real_y_hits[2][k] - mypoint[i].point_1st_2nd_3rd[j][2][t].key) < hit_res_y))
					{
						flag[2] = true;
					}
				}
			}
			for (k = 0; k < real_x_hits[1].size(); k++)
			{
				for (t = 0; t < mypoint[i].point_1st_2nd_3rd[j][1].size(); t++)
				{
					if ((fabs(real_x_hits[1][k] - mypoint[i].point_1st_2nd_3rd[j][1][t].attachment) < hit_res_x) && (fabs(real_y_hits[1][k] - mypoint[i].point_1st_2nd_3rd[j][1][t].key) < hit_res_y))
					{
						flag[1] = true;
					}
				}
			}
			for (k = 0; k < real_x_hits[0].size(); k++)
			{
				for (t = 0; t < mypoint[i].point_1st_2nd_3rd[j][0].size(); t++)
				{
					if ((fabs(real_x_hits[0][k] - mypoint[i].point_1st_2nd_3rd[j][0][t].attachment) < hit_res_x) && (fabs(real_y_hits[0][k] - mypoint[i].point_1st_2nd_3rd[j][0][t].key) < hit_res_y))
					{
						flag[0] = true;
					}
				}
			}
		}
		if (flag[0] && flag[1] && flag[2] && flag[3] && flag[4])
		{
			possible_find_count++;
		}

		for (iter_flag = 0; iter_flag < 5; iter_flag++)
			flag[iter_flag] = false;
		for (iter_x = 0; iter_x < 5; iter_x++)
			real_x_hits[iter_x].clear();
		for (iter_y = 0; iter_y < 5; iter_y++)
			real_y_hits[iter_y].clear();
	}


	//cout << "reconstructable = " << 14153 << endl;
	//cout << "has the possibility that correct = " << possible_find_count << endl;
	//cout << "reconstructed = " << reconstructed << endl;
	//cout << "efficiency = " << double(reconstructed) / 14153.0 << endl;
	//cout << "ghost rate = " << double(reconstructed - possible_find_count) / double(reconstructed) << endl;
	cout << "reconstructable = " << 335 << endl;
	cout << "has the possibility that correct = " << possible_find_count << endl;
	cout << "reconstructed = " << reconstructed << endl;
	cout << "efficiency = " << double(reconstructed) / 335 << endl;
	cout << "ghost rate = " << double(reconstructed - possible_find_count) / double(reconstructed) << endl;
	 
	infiley1.close();
	infiley2.close();
	infiley3.close();
	infiley4.close();
	infiley5.close();
	f->Close();
	delete[] solution;
	return 0;
}

int find(Point *& read_data, float mean, float window, Point *&output_data, int number)
{
	int count = 0, j = 0, i;
	int *note = new int[number + 1];
	float lowerwindow = mean - window;
	float upperwindow = mean + window;
	if (read_data != NULL)
	{
		for (i = 0; i < number; i++)
		{
			if (read_data[i].attachment > lowerwindow && read_data[i].attachment < upperwindow)
			{
				count++;
				note[j] = i;
				j++;
			}
		}
		if (count != 0)
		{
			note[j] = -1;
			output_data = new Point[count];
			for (j = 0; note[j] != -1; j++)
			{
				output_data[j].attachment = read_data[note[j]].attachment;
				output_data[j].key = read_data[note[j]].key;
			}
		}
	}
	delete[] note;
	return count;
}

int find(BinarySearch<float> &data, float mean, float window, Point *&read_data)
{
	int count = 0;
	if (data.BinSearch(mean, window))
	{
		data.Read(read_data);
		//int i;
		count = data.count;
		//for (i = 0; i < data.count; i++)
		//{
		//	count++;
		//}
	}
	else
	{

	}
	return count;
}

template <typename T>
BinarySearch <T>::BinarySearch(ifstream &infile, int max)
{
	int index;
	head = new Point[max];
	infile >> CurrentSize;
	for (index = 0; index < max; index++)
	{
		infile >> head[index].key;
		infile >> head[index].attachment;
	}
	MaxSize = max;
}

template <typename T>
int BinarySearch <T>::BinSearch_Pri(T mean, T window)
{
	int low = 0, high = CurrentSize - 1, mid;
	T upperwindow, lowerwindow;
	upperwindow = mean + window;
	lowerwindow = mean - window;
	while (low <= high)
	{
		mid = low + (high - low) / 2;
		if ((head[low].key > lowerwindow) && (head[low].key < upperwindow))
		{
			return low;
		}
		else if ((head[high].key > lowerwindow) && (head[high].key < upperwindow))
		{
			return high;
		}
		else if ((head[mid].key > lowerwindow) && (head[mid].key < upperwindow))
		{
			return mid;
		}
		else if (head[mid].key <= lowerwindow)
		{
			low = mid + 1;
		}
		else if (head[mid].key >= upperwindow)
		{
			high = mid - 1;
		}
	}
	return -1;
}

template <typename T>
bool BinarySearch <T>::BinSearch(T mean, T window)
{
	int index, temp;
	T lowerwindow, upperwindow;
	lowerwindow = mean - window;
	upperwindow = mean + window;
	index = BinSearch_Pri(mean, window);
	if (index == -1)
	{
		count = 0;
		return false;
	}
	else
	{
		for (temp = index; (head[temp].key > lowerwindow) && (temp >= 0); temp--);
		indexl = temp + 1;
		for (temp = index; (head[temp].key < upperwindow) && (temp <= (CurrentSize - 1)); temp++);
		indexh = temp - 1;
		count = indexh - indexl + 1;
		return true;
	}
}

template <typename T>
bool BinarySearch <T>::Read(Point *&point)
{
	if ((indexh <= CurrentSize) && (indexl >= 0) && (count != 0))
	{
		int i;
		point = new Point[count];
		for (i = 0; i < count; i++)
		{	
			point[i].key = head[indexl + i].key;
			point[i].attachment = head[indexl + i].attachment;
		}
		return true;
	}
	else
		return false;
}

// [0]-a [1]-b [2]-c
void get_abc(float x3, float x4, float x5, float * &solution)
{
	//c
	solution[2] = (8717.0*x5*x5 - 9402.0*x3*x3 - (9193 * x5*x5 - 9402 * x4*x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4)) / (x5*x5 - x3 * x3 - (x5*x5 - x4 * x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4));
	//b
	solution[1] = (1 / (x4 - x4 * x4 / x5))*(9193 - 9402 * x4 * x4 / (x5 * x5) - (1 - x4 * x4 / (x5 * x5)) * solution[2]);
	//a
	solution[0] = (1 / (x5*x5))*(9402 - x5 * solution[1] - solution[2]);
}

bool decide(float x3, float x4, float x5, float middle)
{
	bool com1 = 0, com2 = 0, com3 = 0;
	if (x3 > middle)
		com1 = 1;
	else
		com1 = 0;
	if (x4 > middle)
		com2 = 1;
	else
		com2 = 0;
	if (x5 > middle)
		com3 = 1;
	else
		com3 = 0;
	return (com1 || com2 || com3) && (!(com1 && com2 && com3));
}

bool happened(double probability)//probability 0~1
{
	if (probability <= 0)
	{
		return false;
	}
	if (probability < MinProb)
	{
		return rand() == 0 && happened(probability*(long(RAND_MAX) + 1));
	}
	if (rand() <= probability * (long(RAND_MAX) + 1))
	{
		return true;
	}
	return false;
}

int myrandom(int n)//产生0~n-1之间的等概率随机数
{
	int t = 0;
	if (n <= RAND_MAX)
	{
		int R = RAND_MAX - (long(RAND_MAX) + 1) % n;//尾数
		t = rand();
		while (t > R)
		{
			t = rand();
		}
		return t % n;
	}
	else
	{
		int r = n % (long(RAND_MAX) + 1);//余数
		if (happened((double)r / n))//取到余数的概率
		{
			return n - r + myrandom(r);
		}
		else
		{
			return rand() + myrandom(n / (long(RAND_MAX) + 1))*(long(RAND_MAX) + 1);
		}
	}
}
