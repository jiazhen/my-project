# include <iostream>
using namespace std;

void work6()
{
	TFile f("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");
	TH1F *delta_y = new TH1F("delta y between predicted position and true position on layer 5", "delta y between predicted position and true position on layer 5", 800000, -20000, 20000);
	float hitzpos[30], hitypos[30], y[6], pre_y[4], z = 0;
	double chi2, ndf, con;
	y[0] = 0; //layer 1
	y[1] = 0; //layer 2
	y[2] = 0; //layer 3
	y[3] = 0; //layer 4
	y[4] = 0; //layer 5
	y[5] = 0; //layer 6
	pre_y[0] = 0; //predicted y layer 1
	pre_y[1] = 0; //predicted y layer 2
	pre_y[2] = 0; //predicted y layer 3
	pre_y[3] = 0; //predicted y layer 4
	int nentries, i, j, count1 = 0, count2 = 0, count3 = 0, count4 = 0, count5 = 0, count6 = 0, count = 0;
	bool flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0, flag5 = 0, flag6 = 0;
	double p, pz, eta;
	tracks->SetBranchAddress("p", &p);
	tracks->SetBranchAddress("pz", &pz);
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	nentries = tracks->GetEntries();
	for (i = 0; i < nentries; i++)
	{
		tracks->GetEntry(i);
		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 7816 && hitzpos[j] < 7836)
			{
				count1++;
				y[0] = y[0] + hitypos[j];
				flag1 = 1;
			}
			else if (hitzpos[j] > 8024 && hitzpos[j] < 8047)
			{
				count2++;
				y[1] = y[1] + hitypos[j];
				flag2 = 1;
			}
			else if (hitzpos[j] > 8497 && hitzpos[j] < 8518)
			{
				count3++;
				y[2] = y[2] + hitypos[j];
				flag3 = 1;
			}
			else if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				count4++;
				y[3] = y[3] + hitypos[j];
				flag4 = 1;
			}
			else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
			{
				count5++;
				y[4] = y[4] + hitypos[j];
				flag5 = 1;
			}
			else if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
			{
				count6++;
				y[5] = y[5] + hitypos[j];
				flag6 = 1;
			}
		}
		eta = 0.5*log((p + pz) / (p - pz));
		if (p > 5 && (eta > 2 && eta < 5))
		{
			if (flag1 && flag2 && flag3 && flag4 && flag5 && flag6)
			{
				if (count1 > 1 || count2 > 1 || count3 > 1 || count4 > 1 || count5 > 1 || count6 > 1)
				{

				}
				else
				{
					//count++;
					//x[0] = x[0] / count1;
					//x[1] = x[1] / count2;
					//x[2] = x[2] / count3;
					//x[3] = x[3] / count4;
					//x[4] = x[4] / count5;
					//x[5] = x[5] / count6;

					////pre_x[0] = (1576.0 / 209.0)*x[4] - (1367.0 / 209.0)*x[5];
					////pre_x[1] = (1367.0 / 209.0)*x[4] - (1158.0 / 209.0)*x[5];
					////pre_x[2] = (894.0 / 209.0)*x[4] - (685.0 / 209.0)*x[5];
					//pre_x[3] = (685.0 / 209.0)*x[4] - (476.0 / 209.0)*x[5];

					////delta_x->Fill(x[5] - x[4]);
					////delta_x->Fill(pre_x[0] - x[0]);
					////delta_x->Fill(pre_x[1] - x[1]);
					////delta_x->Fill(pre_x[2] - x[2]);
					//delta_x->Fill(pre_x[3] - x[3]);
					y[0] = y[0] / count1;
					y[1] = y[1] / count2;
					y[2] = y[2] / count3;
					y[3] = y[3] / count4;
					y[4] = y[4] / count5;
					y[5] = y[5] / count6;
					pre_y[0] = (1576.0 / 209.0)*y[4] - (1367.0 / 209.0)*y[5];
					pre_y[1] = (1367.0 / 209.0)*y[4] - (1158.0 / 209.0)*y[5];
					pre_y[2] = (894.0 / 209.0)*y[4] - (685.0 / 209.0)*y[5];
					pre_y[3] = (685.0 / 209.0)*y[4] - (476.0 / 209.0)*y[5];
					count++;
					delta_y->Fill(y[5] - y[4]);
					//delta_y->Fill(pre_y[0] - y[0]);
					//delta_y->Fill(pre_y[1] - y[1]);
					//delta_y->Fill(pre_y[2] - y[2]);
					//delta_y->Fill(pre_y[3] - y[3]);
				}

			}
		}

		count1 = 0;
		count2 = 0;
		count3 = 0;
		count4 = 0;
		count5 = 0;
		count6 = 0;
		y[0] = 0;
		y[1] = 0;
		y[2] = 0;
		y[3] = 0;
		y[4] = 0;
		y[5] = 0;
		pre_y[0] = 0;
		pre_y[1] = 0;
		pre_y[2] = 0;
		pre_y[3] = 0;
		flag1 = 0;
		flag2 = 0;
		flag3 = 0;
		flag4 = 0;
		flag5 = 0;
		flag6 = 0;
	}
	cout << "count = " << count << endl;
	gStyle->SetOptStat(0);
	//gStyle->SetOptFit(1111);
	delta_y->Fit("gaus");
	TFitResultPtr fit_res = delta_y->Fit("gaus", "S");
	TCanvas *c1 = new TCanvas("c1", "A Canvas", 10, 10, 1600, 900);
	chi2 = fit_res->Chi2();
	ndf = fit_res->Ndf();
	con = chi2 / ndf;
	cout << "chi2 = " << chi2 << endl;
	cout << "NDF = " << ndf << endl;
	cout << "chi2/NDF = " << con << endl;
	delta_y->Draw();
	delta_y->GetXaxis()->SetRangeUser(-100, 100);
	delta_y->GetXaxis()->SetTitle("delta y/mm");
	c1->Print("layery5.gif");
	//c1->SaveAs("work6.pdf");
	c1->Modified();
	c1->Update();
	f.Close();
}
