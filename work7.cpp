# include <iostream>
# include <cmath>
using namespace std;

// [0]-a [1]-b [2]-c
void function(float x3, float x4, float x5, float * &solution)
{
	//c
	solution[2] = (8717.0*x5*x5 - 9402.0*x3*x3 - (9193 * x5*x5 - 9402 * x4*x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4)) / (x5*x5 - x3 * x3 - (x5*x5 - x4 * x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4));
	//b
	solution[1] = (1 / (x4 - x4 * x4 / x5))*(9193 - 9402 * x4 * x4 / (x5 * x5) - (1 - x4 * x4 / (x5 * x5)) * solution[2]);
	//a
	solution[0] = (1 / (x5*x5))*(9402 - x5 * solution[1] - solution[2]);
}

void work7()
{
	TFile f("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");
	TH1F *delta_x = new TH1F("delta_x", "delta x between point predicted by a line and true point on layer 1", 1200000, -20000, 20000);
	float hitzpos[30], hitxpos[30], x[6], pre_x[4], z = 0;
	double chi2, ndf, con;
	x[0] = 0; //layer 1
	x[1] = 0; //layer 2
	x[2] = 0; //layer 3
	x[3] = 0; //layer 4
	x[4] = 0; //layer 5
	x[5] = 0; //layer 6
	pre_x[0] = 0; //predicted x layer 1
	pre_x[1] = 0; //predicted x layer 2
	pre_x[2] = 0; //predicted x layer 3
	pre_x[3] = 0; //predicted x layer 4
	int nentries, i, j, count1 = 0, count2 = 0, count3 = 0, count4 = 0, count5 = 0, count6 = 0, count = 0;
	bool flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0, flag5 = 0, flag6 = 0;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitXpos", hitxpos);

	int abnormal_number = 0, cut_number = 0;
	double p, pz, eta;
	tracks->SetBranchAddress("p", &p);
	tracks->SetBranchAddress("pz", &pz);

	nentries = tracks->GetEntries();
	for (i = 0; i < nentries; i++)
	{
		tracks->GetEntry(i);
		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 7816 && hitzpos[j] < 7836)
			{
				count1++;
				x[0] = x[0] + hitxpos[j];
				flag1 = 1;
			}
			else if (hitzpos[j] > 8024 && hitzpos[j] < 8047)
			{
				count2++;
				x[1] = x[1] + hitxpos[j];
				flag2 = 1;
			}
			else if (hitzpos[j] > 8497 && hitzpos[j] < 8518)
			{
				count3++;
				x[2] = x[2] + hitxpos[j];
				flag3 = 1;
			}
			else if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				count4++;
				x[3] = x[3] + hitxpos[j];
				flag4 = 1;
			}
			else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
			{
				count5++;
				x[4] = x[4] + hitxpos[j];
				flag5 = 1;
			}
			else if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
			{
				count6++;
				x[5] = x[5] + hitxpos[j];
				flag6 = 1;
			}
		}
		eta = 0.5*log((p + pz) / (p - pz));
		if (p > 5 && (eta > 2 && eta < 5))
		{
			cut_number++;
			if (flag1 && flag2 && flag3 && flag4 && flag5 && flag6)
			{
				if (count1 > 1 || count2 > 1 || count3 > 1 || count4 > 1 || count5 > 1 || count6 > 1)
				{
					abnormal_number++;
				}
				else
				{
					count++;
					x[0] = x[0] / count1;
					x[1] = x[1] / count2;
					x[2] = x[2] / count3;
					x[3] = x[3] / count4;
					x[4] = x[4] / count5;
					x[5] = x[5] / count6;

					pre_x[0] = (1576.0 / 209.0)*x[4] - (1367.0 / 209.0)*x[5];
					pre_x[1] = (1367.0 / 209.0)*x[4] - (1158.0 / 209.0)*x[5];
					pre_x[2] = (894.0 / 209.0)*x[4] - (685.0 / 209.0)*x[5];
					pre_x[3] = (685.0 / 209.0)*x[4] - (476.0 / 209.0)*x[5];

					delta_x->Fill(pre_x[0] - x[0]);
					//delta_x->Fill(pre_x[1] - x[1]);
					//delta_x->Fill(pre_x[2] - x[2]);
					//delta_x->Fill(pre_x[3] - x[3]);
				}
			}
		}
		count1 = 0;
		count2 = 0;
		count3 = 0;
		count4 = 0;
		count5 = 0;
		count6 = 0;
		x[0] = 0;
		x[1] = 0;
		x[2] = 0;
		x[3] = 0;
		x[4] = 0;
		x[5] = 0;
		pre_x[0] = 0;
		pre_x[1] = 0;
		pre_x[2] = 0;
		pre_x[3] = 0;
		flag1 = 0;
		flag2 = 0;
		flag3 = 0;
		flag4 = 0;
		flag5 = 0;
		flag6 = 0;
	}
	cout << "cut number = " << cut_number << endl;
	cout << "abnormal number = " << abnormal_number << endl;
	cout << "count = " << count << endl;
	gStyle->SetOptStat(0);
	//gStyle->SetOptFit(1111);
	delta_x->Fit("gaus");
	TFitResultPtr fit_res = delta_x->Fit("gaus", "S");
	chi2 = fit_res->Chi2();
	ndf = fit_res->Ndf();
	con = chi2 / ndf;
	cout << "chi2 = " << chi2 << endl;
	cout << "NDF = " << ndf << endl;
	cout << "chi2/NDF = " << con << endl;
	TCanvas *c1 = new TCanvas("c1", "A Canvas", 10, 10, 1600, 900);
	delta_x->Draw();
	delta_x->GetXaxis()->SetRangeUser(-100, 100);
	c1->Print("layerx1.gif");
	//c1->SaveAs("work7.pdf");
	c1->Modified();
	c1->Update();
	f.Close();
}
