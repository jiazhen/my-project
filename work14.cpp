# include<cmath>
# include<vector>
# include<fstream>
# include<iostream>
using namespace std;

void get_abc(float x3, float x4, float x5, float* & solution);

int work14()
{
	TFile *f = TFile::Open("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");
	//TH1F *diff_slope = new TH1F("difference of the slope", "difference of the slope", 100000, -100, 100);
	ifstream infile("candidate track.txt", ios::in);
	ofstream outfile("candidate track with decide for read.txt", ios::out);
	float hitzpos[30], hitypos[30], hitxpos[30];
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitXpos", hitxpos);
	tracks->SetBranchAddress("HitYpos", hitypos);

	float x[6], y[6];
	vector<float> real_x_hits[5], real_y_hits[5];
	float k1, k5;
	float max = 0, min = 0;
	int i, entry, index, iter_xy, j, k, iter_x, iter_y, iter_flag;
	bool result = false;
	bool flag[5] = { false, false, false, false, false };
	float hit_res_x = (0.1 / sqrt(12));
	float hit_res_y = (0.4 / sqrt(12));
	int possible_find_count = 0;
	for (i = 0; i < 52420; i++)
	{
		infile >> index;
		infile >> entry;
		for (iter_xy = 5; iter_xy >= 0; iter_xy--)
		{
			infile >> y[iter_xy];
			infile >> x[iter_xy];
		}
		//k5 = (9402.0 - 9193.0) / (x[5] - x[4]);
		//k1 = (8035.0 - 7826.0) / (x[1] - x[0]);
		////diff_slope->Fill(k5 - k1);
		//if (max < (k5 - k1))
		//{
		//	max = k5 - k1;
		//}
		//if (min > (k5 - k1))
		//{
		//	min = k5 - k1;
		//}

		tracks->GetEntry(entry);
		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 7816 && hitzpos[j] < 7836)
			{
				real_x_hits[0].push_back(hitxpos[j]);
				real_y_hits[0].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 8024 && hitzpos[j] < 8047)
			{
				real_x_hits[1].push_back(hitxpos[j]);
				real_y_hits[1].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 8497 && hitzpos[j] < 8518)
			{
				real_x_hits[2].push_back(hitxpos[j]);
				real_y_hits[2].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				real_x_hits[3].push_back(hitxpos[j]);
				real_y_hits[3].push_back(hitypos[j]);
			}
			else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
			{
				real_x_hits[4].push_back(hitxpos[j]);
				real_y_hits[4].push_back(hitypos[j]);
			}
		}

		for (k = 0; k < real_x_hits[4].size(); k++)
		{
			if ((fabs(real_x_hits[4][k] - x[4]) < hit_res_x) && (fabs(real_y_hits[4][k] - y[4]) < hit_res_y))
			{
				flag[4] = true;
			}
		}
		for (k = 0; k < real_x_hits[3].size(); k++)
		{
			if ((fabs(real_x_hits[3][k] - x[3]) < hit_res_x) && (fabs(real_y_hits[3][k] - y[3]) < hit_res_y))
			{
				flag[3] = true;
			}
		}
		for (k = 0; k < real_x_hits[2].size(); k++)
		{
			if ((fabs(real_x_hits[2][k] - x[2]) < hit_res_x) && (fabs(real_y_hits[2][k] - y[2]) < hit_res_y))
			{
				flag[2] = true;
			}
		}
		for (k = 0; k < real_x_hits[1].size(); k++)
		{
			if ((fabs(real_x_hits[1][k] - x[1]) < hit_res_x) && (fabs(real_y_hits[1][k] - y[1]) < hit_res_y))
			{
				flag[1] = true;
			}
		}
		for (k = 0; k < real_x_hits[0].size(); k++)
		{
			if ((fabs(real_x_hits[0][k] - x[0]) < hit_res_x) && (fabs(real_y_hits[0][k] -y[0]) < hit_res_y))
			{
				flag[0] = true;
			}
		}


		if (flag[0] && flag[1] && flag[2] && flag[3] && flag[4])
		{
			possible_find_count++;
			result = true;
		}

		outfile << index << " ";
		outfile << entry << " ";
		outfile << result << " ";
		for (iter_xy = 0; iter_xy < 6; iter_xy++)
		{
			outfile << y[iter_xy] << " ";
			outfile << x[iter_xy] << " ";
		}
		outfile << endl;

		for (iter_flag = 0; iter_flag < 5; iter_flag++)
			flag[iter_flag] = false;
		for (iter_x = 0; iter_x < 5; iter_x++)
			real_x_hits[iter_x].clear();
		for (iter_y = 0; iter_y < 5; iter_y++)
			real_y_hits[iter_y].clear();
		result = false;
	}
	cout << possible_find_count << endl;
	//TCanvas *c1 = new TCanvas("c1", "A Canvas", 10, 10, 1600, 900);
	//diff_slope->Draw();
	//c1->Print("difference of the slope.gif");
	//c1->Modified();
	//c1->Update();
	return 0;
}

// [0]-a [1]-b [2]-c
void get_abc(float x3, float x4, float x5, float * &solution)
{
	//c
	solution[2] = (8717.0*x5*x5 - 9402.0*x3*x3 - (9193 * x5*x5 - 9402 * x4*x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4)) / (x5*x5 - x3 * x3 - (x5*x5 - x4 * x4)*(x3*x5 - x3 * x3) / (x4*x5 - x4 * x4));
	//b
	solution[1] = (1 / (x4 - x4 * x4 / x5))*(9193 - 9402 * x4 * x4 / (x5 * x5) - (1 - x4 * x4 / (x5 * x5)) * solution[2]);
	//a
	solution[0] = (1 / (x5*x5))*(9402 - x5 * solution[1] - solution[2]);
}
