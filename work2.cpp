# include <iostream>
using namespace std;
void work2()
{
    TFile f("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
    TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
    TTree* tracks = (TTree*)dir->Get("Tracks");
    float hitzpos[30], hitypos[30];
    int nentries, i, j, count = 0;
    tracks->SetBranchAddress("HitZpos", hitzpos);
    tracks->SetBranchAddress("HitYpos", hitypos);
    nentries = tracks->GetEntries();
    TH2F *zyaxis = new TH2F("zyaxis", "hitzypos", 20000, 7700, 9500, 50000, -2630, 2680);
    for(i = 0; i < nentries; i++)
      {
	tracks->GetEntry(i);
	for(j = 0; j < 30; j++)
	  {
	    if(hitzpos[j] > 7700 && hitzpos[j] < 9500)
	      {
		zyaxis->Fill(hitzpos[j], hitypos[j]);
		count++;
	      }
	  }
      }
    cout << count << endl;
    TCanvas *c1 = new TCanvas("c1", "A Canvas", 10, 10, 800, 600);
    zyaxis->Draw();
    c1->Print("work2.gif");
    c1->SaveAs("work2.pdf");
    c1->Modified();
    c1->Update();
}
