# include<cmath>
# include<fstream>
# include<iostream>
using namespace std;
struct Point
{
	float key;
	float attachment;
};
class MinHeap
{
public:
	MinHeap(float * &key, float * &attachment, int n);
	~MinHeap() { delete[] this->heap; }
	bool Insert(const Point & x);
	bool RemoveMin(Point & x);
	bool IsEmpty()const { return(currentSize == 0) ? true : false; }
	bool eject()const { return(currentSize == 1) ? true : false; }
	bool Write(ofstream &outfile);
	int size()const { return currentSize; }
private:
	Point *heap;
	int currentSize;
	int maxHeapSize;
	void siftDown(int start, int m);
	void siftUp(int start);
};
void work9()
{
	TFile f("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");
	float hitzpos[30], hitypos[30], hitxpos[30];
	int i, j, nentries;
	int count1 = 0, count2 = 0, count3 = 0, count4 = 0, count5 = 0, count6 = 0;
	double eta, p, pz;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitXpos", hitxpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	tracks->SetBranchAddress("p", &p);
	tracks->SetBranchAddress("pz", &pz);
	nentries = tracks->GetEntries();

	ofstream outfiley1("layer1 y hits.txt", ios::out);
	ofstream outfiley2("layer2 y hits.txt", ios::out);
	ofstream outfiley3("layer3 y hits.txt", ios::out);
	ofstream outfiley4("layer4 y hits.txt", ios::out);
	ofstream outfiley5("layer5 y hits.txt", ios::out);
	ofstream outfiley6("layer6 y hits.txt", ios::out);

	ofstream outfilex1("layer1 x hits.txt", ios::out);
	ofstream outfilex2("layer2 x hits.txt", ios::out);
	ofstream outfilex3("layer3 x hits.txt", ios::out);
	ofstream outfilex4("layer4 x hits.txt", ios::out);
	ofstream outfilex5("layer5 x hits.txt", ios::out);
	ofstream outfilex6("layer6 x hits.txt", ios::out);

	float* hity1st = new float[35071];
	float* hity2nd = new float[37621];
	float* hity3rd = new float[33965];
	float* hity4th = new float[38479];
	float* hity5th = new float[37056];
	float* hity6th = new float[41873];

	float* hitx1st = new float[35071];
	float* hitx2nd = new float[37621];
	float* hitx3rd = new float[33965];
	float* hitx4th = new float[38479];
	float* hitx5th = new float[37056];
	float* hitx6th = new float[41873];

	int index[7] = { 0, 0, 0, 0, 0, 0, 0 };

	for (i = 0; i < nentries; i++)
	{
		tracks->GetEntry(i);
		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 7816 && hitzpos[j] < 7836)
			{
				count1++;
				hity1st[index[1]] = hitypos[j];
				hitx1st[index[1]] = hitxpos[j];
				index[1]++;
			}
			else if (hitzpos[j] > 8024 && hitzpos[j] < 8047)
			{
				count2++;
				hity2nd[index[2]] = hitypos[j];
				hitx2nd[index[2]] = hitxpos[j];
				index[2]++;
			}
			else if (hitzpos[j] > 8497 && hitzpos[j] < 8518)
			{
				count3++;
				hity3rd[index[3]] = hitypos[j];
				hitx3rd[index[3]] = hitxpos[j];
				index[3]++;
			}
			else if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				count4++;
				hity4th[index[4]] = hitypos[j];
				hitx4th[index[4]] = hitxpos[j];
				index[4]++;
			}
			else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
			{
				count5++;
				hity5th[index[5]] = hitypos[j];
				hitx5th[index[5]] = hitxpos[j];
				index[5]++;
			}
			else if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
			{
				count6++;
				hity6th[index[6]] = hitypos[j];
				hitx6th[index[6]] = hitxpos[j];
				index[6]++;
			}
		}
	}
	MinHeap y1stlayer(hity1st, hitx1st, 35071);
	MinHeap y2ndlayer(hity2nd, hitx2nd, 37621);
	MinHeap y3rdlayer(hity3rd, hitx3rd, 33965);
	MinHeap y4thlayer(hity4th, hitx4th, 38479);
	MinHeap y5thlayer(hity5th, hitx5th, 37056);
	MinHeap y6thlayer(hity6th, hitx6th, 41873);

	MinHeap x1stlayer(hitx1st, hity1st, 35071);
	MinHeap x2ndlayer(hitx2nd, hity2nd, 37621);
	MinHeap x3rdlayer(hitx3rd, hity3rd, 33965);
	MinHeap x4thlayer(hitx4th, hity4th, 38479);
	MinHeap x5thlayer(hitx5th, hity5th, 37056);
	MinHeap x6thlayer(hitx6th, hity6th, 41873);

	y1stlayer.Write(outfiley1);
	y2ndlayer.Write(outfiley2);
	y3rdlayer.Write(outfiley3);
	y4thlayer.Write(outfiley4);
	y5thlayer.Write(outfiley5);
	y6thlayer.Write(outfiley6);

	x1stlayer.Write(outfilex1);
	x2ndlayer.Write(outfilex2);
	x3rdlayer.Write(outfilex3);
	x4thlayer.Write(outfilex4);
	x5thlayer.Write(outfilex5);
	x6thlayer.Write(outfilex6);

	outfiley1.close();
	outfiley2.close();
	outfiley3.close();
	outfiley4.close();
	outfiley5.close();
	outfiley6.close();

	outfilex1.close();
	outfilex2.close();
	outfilex3.close();
	outfilex4.close();
	outfilex5.close();
	outfilex6.close();

	cout << "hits on layer 1 = " << count1 << endl;
	cout << "hits on layer 2 = " << count2 << endl;
	cout << "hits on layer 3 = " << count3 << endl;
	cout << "hits on layer 4 = " << count4 << endl;
	cout << "hits on layer 5 = " << count5 << endl;
	cout << "hits on layer 6 = " << count6 << endl;
	//tracks->GetReadEntry()

	f.Close();
	delete[] hity1st;
	delete[] hity2nd;
	delete[] hity3rd;
	delete[] hity4th;
	delete[] hity5th;
	delete[] hity6th;
	delete[] hitx1st;
	delete[] hitx2nd;
	delete[] hitx3rd;
	delete[] hitx4th;
	delete[] hitx5th;
	delete[] hitx6th;
}

MinHeap::MinHeap(float * & key, float * & attachment, int n)
{
	maxHeapSize = (10 < n) ? n : 10;
	heap = new Point[maxHeapSize];
	int i = 0;
	for (i = 0; i < n; i++)
	{
		heap[i].key = key[i];
		heap[i].attachment = attachment[i];
	}
	currentSize = n;
	int currentPos = (currentSize - 2) / 2;
	while (currentPos >= 0)
	{
		siftDown(currentPos, currentSize - 1);
		currentPos--;
	}
};

void MinHeap::siftDown(int start, int m)
{
	int i = start, j = 2 * i + 1;
	float temp = heap[i].key;
	float attach = heap[i].attachment;
	while (j <= m)
	{
		if (j < m && heap[j].key > heap[j + 1].key)
			j++;
		if (temp <= heap[j].key)
			break;
		else
		{
			heap[i].key = heap[j].key;
			heap[i].attachment = heap[j].attachment;
			i = j;
			j = 2 * j + 1;
		}
	}
	heap[i].key = temp;
	heap[i].attachment = attach;
};

void MinHeap::siftUp(int start)
{
	int j = start, i = (j - 1) / 2;
	float temp = heap[j].key;
	float attach = heap[i].attachment;
	while (j > 0)
	{
		if (heap[i].key <= temp)
			break;
		else
		{
			heap[j].key = heap[i].key;
			heap[j].attachment = heap[i].attachment;
			j = i;
			i = (i - 1) / 2;
		}
		heap[j].key = temp;
		heap[j].attachment = attach;
	}
};

bool MinHeap::Insert(const Point& x)
{
	if (currentSize == maxHeapSize)
	{
		Point * tempary, *newheap;
		maxHeapSize += 10000;
		newheap = new Point[maxHeapSize];
		int i = 0;
		for (i = 0; i < currentSize; i++)
		{
			newheap[i].key = heap[i].key;
			newheap[i].attachment = newheap[i].attachment;
		}
		tempary = heap;
		heap = newheap;
		delete[]tempary;
	}
	heap[currentSize].key = x.key;
	heap[currentSize].attachment = x.attachment;
	siftUp(currentSize);
	currentSize++;
	return true;
}

bool MinHeap::RemoveMin(Point & x)
{
	if (!currentSize)
		return false;
	x.attachment = heap[0].attachment;
	x.key = heap[0].key;
	heap[0].attachment = heap[currentSize - 1].attachment;
	heap[0].key = heap[currentSize - 1].key;
	currentSize--;
	siftDown(0, currentSize - 1);
	return true;
}

bool MinHeap::Write(ofstream &outfile)
{
	int i = 0;
	Point minimum;
	while (!IsEmpty())
	{
		i++;
		RemoveMin(minimum);
		outfile << minimum.key << " " << minimum.attachment << " ";
	}
	cout << i << endl;
	return true;
}
