# include <iostream>
using namespace std;
void asdf()
{
	TFile f("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree* tracks = (TTree*)dir->Get("Tracks");
	float hitzpos[30], hitypos[30], hitxpos[30];
	int nentries, i, j, count = 0;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	tracks->SetBranchAddress("HitXpos", hitxpos);
	nentries = tracks->GetEntries();
	TH2F *zyaxis = new TH2F("zyaxis", "y x position", 50000, -2630, 2680, 20000, -5000, 5000);
	for (i = 0; i < nentries; i++)
	{
		tracks->GetEntry(i);
		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				zyaxis->Fill(hitypos[j], hitxpos[j]);
				count++;
			}
		}
	}
	cout << count << endl;
	TCanvas *c1 = new TCanvas("c1", "A Canvas", 10, 10, 1600, 900);
	zyaxis->Draw();
	c1->Print("layer.gif");
	//c1->SaveAs("layer.pdf");
	c1->Modified();
	c1->Update();
}
