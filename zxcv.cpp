# include<fstream>
# include<iostream>
using namespace std;

int zxcv()
{
	TFile *f = TFile::Open("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");

	ofstream outfile("csearch entry number.txt", ios::out);

	float hitzpos[30], hitxpos[30], hitypos[30];
	int nentries, count = 0, i, j;
	double p, pz, eta;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitXpos", hitxpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	tracks->SetBranchAddress("p", &p);
	tracks->SetBranchAddress("pz", &pz);
	nentries = tracks->GetEntries();
	bool flag[7] = { 0, 0, 0, 0, 0, 0, 0 };
	int iter_flag;

	for (i = 0; i < nentries; i++)
	{
		tracks->GetEntry(i);
		eta = 0.5*log((p + pz) / (p - pz));
		if (p > 5 && (eta > 2 && eta < 5))
		{
			for (j = 0; j < 30; j++)
			{
				if (hitzpos[j] > 7816 && hitzpos[j] < 7836)
				{
					flag[1] = 1;
				}
				else if (hitzpos[j] > 8024 && hitzpos[j] < 8047)
				{
					flag[2] = 1;
				}
				else if (hitzpos[j] > 8497 && hitzpos[j] < 8518)
				{
					flag[3] = 1;
				}
				else if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
				{
					flag[4] = 1;
				}
				else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
				{
					flag[5] = 1;
				}
				else if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
				{
					flag[6] = 1;
				}
			}
			if (flag[1] && flag[2] && flag[3] && flag[4] && flag[5] && flag[6])
			{
				for (j = 0; j < 30; j++)
				{

					if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
					{
						count++;
						outfile << i << " ";
						outfile << hitxpos[j] << " ";
						outfile << hitypos[j] << " ";
					}
				}
			}
		}
		for (iter_flag = 1; iter_flag < 7; iter_flag++)
			flag[iter_flag] = 0;
	}

	cout << "count = " << count;
	return 0;
}
