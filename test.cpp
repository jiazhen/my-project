# include <fstream>
using namespace std;
void test()
{
	TFile f("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");
	TH1F *delta_y;
	ofstream file;
	file.open("abnormal hit on 6 layer.txt");
	float hitzpos[30], hitypos[30], y[4], z = 0;
	y[0] = 0; //layer 4
	y[1] = 0; //layer 5
	y[2] = 0; //layer 6
	y[3] = 0; //predicted y
	int nentries, i, j, count4 = 0, count5 = 0, count6 = 0, count = 0;
	bool flag4 = 0, flag5 = 0, flag6 = 0;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	nentries = tracks->GetEntries();
	int asdf = 0; //输出y坐标
	for (i = 0; i < nentries; i++)
	{
		tracks->GetEntry(i);
		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				count4++;
				y[0] = y[0] + hitypos[j];
				flag4 = 1;
			}
			if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
			{
				count5++;
				y[1] = y[1] + hitypos[j];
				flag5 = 1;
			}
			else if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
			{
				count6++;
				y[2] = y[2] + hitypos[j];
				flag6 = 1;
			}
		}
		if (flag4 && flag5 && flag6)
		{
			y[0] = y[0] / count4;
			y[1] = y[1] / count5;
			y[2] = y[2] / count6;
			y[3] = -(476.0 / 209.0)*y[2] + (685.0 / 209.0)*y[1];
			if (count6 > 1)/*(count4 > 1|| count5 > 1 || count6 > 1)*/
			{
				count++;
				file << "hit on layer 6: " << count6 << endl;
				for (j = 0; j < 30; j++)
				{
					if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
					{
						asdf++;
						file << "y" << asdf << " position : " << hitypos[j] << "  j = " << j << endl;
					}
				}
				//file << "hit on layer 5: " << count5 << endl;
				//file << "hit on layer 6: " << count6 << endl;
			}
		}
		count4 = 0;
		count5 = 0;
		count6 = 0;
		y[0] = 0;
		y[1] = 0;
		y[2] = 0;
		y[3] = 0;
		asdf = 0;
		flag4 = 0;
		flag5 = 0;
		flag6 = 0; 
	}
	file << "count = " << count;
	file.close();
	f.Close();
}

