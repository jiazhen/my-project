# include <iostream>
using namespace std;
void work3()
{
	TFile f("/eos/lhcb/user/i/icortino/MightyIT/MCtracks_MagUp_bs2phiphi_1p5e34_nocuts_20ev.root");
	TDirectoryFile *dir = (TDirectoryFile*)gDirectory->Get("MCParticleNTuple");
	TTree *tracks = (TTree*)dir->Get("Tracks");
	TH1F *delta_y= new TH1F("delta_y", "delta_y", 17000000, -20000, 20000);
	float hitzpos[30], hitypos[30], y[4], z = 0;
	y[0] = 0; //layer 4
	y[1] = 0; //layer 5
	y[2] = 0; //layer 6
	y[3] = 0; //predicted y
	int nentries, i, j, count4 = 0, count5 = 0, count6 = 0, count = 0;
	bool flag4 = 0, flag5 = 0, flag6 = 0;
	tracks->SetBranchAddress("HitZpos", hitzpos);
	tracks->SetBranchAddress("HitYpos", hitypos);
	nentries = tracks->GetEntries();
	for (i = 0; i < nentries; i++)
	{
		tracks->GetEntry(i);
		for (j = 0; j < 30; j++)
		{
			if (hitzpos[j] > 8706 && hitzpos[j] < 8728)
			{
				count4++;
				y[0] = y[0] + hitypos[j];
				flag4 = 1;
			}
			else if (hitzpos[j] > 9183 && hitzpos[j] < 9203)
			{
				count5++;
				y[1] = y[1] + hitypos[j];
				flag5 = 1;
			}
			else if (hitzpos[j] > 9391 && hitzpos[j] < 9414)
			{
				count6++;
				y[2] = y[2] + hitypos[j];
				flag6 = 1;
			}
		}
		if (flag4 && flag5 && flag6)
		{
			y[0] = y[0] / count4;
			y[1] = y[1] / count5;
			y[2] = y[2] / count6;
			y[3] = -(476.0 / 209.0)*y[2] + (685.0 / 209.0)*y[1];
			count++;
			delta_y->Fill(y[3] - y[0]);
		}
		count4 = 0;
		count5 = 0;
		count6 = 0;
		y[0] = 0;
		y[1] = 0;
		y[2] = 0;
		y[3] = 0;
		flag4 = 0;
		flag5 = 0;
		flag6 = 0;
	}
	cout << count << endl;
	TCanvas *c1 = new TCanvas("c1", "A Canvas", 10, 10, 1600, 900);
	delta_y->Draw();
	delta_y->GetXaxis()->SetRangeUser(-20, 20);
	gStyle->SetOptFit(1111);
	delta_y->Fit("gaus");
	TFitResultPtr fit_res = delta_y->Fit("gaus","S");
	double chi2 = fit_res->Chi2();
	int ndf = fit_res->Ndf();
	cout << "chi2 = " << chi2 << endl;
	cout << "NDF = " << ndf << endl;
	cout << "chi2/NDF = " << chi2/ndf << endl;
	c1->Print("work3-2.gif");
	c1->SaveAs("work3-2.pdf");
	c1->Modified();
	c1->Update();
	f.Close();
}
